export const TYPE = {
    STRING: 'string',
    NUMBER: 'number',
    ARRAY: 'array',
    FUNCTION: 'function',
    OBJECT: 'object',
    BOOLEAN: 'boolean',
    NULL: 'null',
    UNDEFINED: 'undefined',
};

const TypeHelper = {
    isString: (value: any) => typeof value === 'string' || value instanceof String,
    isNumber: (value: any) => typeof value === 'number' && isFinite(value),
    isArray: (value: any) => value && typeof value === 'object' && value.constructor === Array,
    isFunction: (value: any) => typeof value === 'function',
    isObject: (value: any) => value && typeof value === 'object' && value.constructor === Object,
    isNull: (value: any) => value === null,
    isUndefined: (value: any) => typeof value === 'undefined',
    isBoolean: (value: any) => typeof value === 'boolean',
    isRegExp: (value: any) => value && typeof value === 'object' && value.constructor === RegExp,
    isError: (value: any) => value instanceof Error && typeof value.message !== 'undefined',
    isSymbol: (value: any) => typeof value === 'symbol',
    isDate: (value: any) => value instanceof Date,
    getType(value: any){
        if(this.isString(value))
            return TYPE.STRING;
        if(this.isNumber(value))
            return TYPE.NUMBER;
        if(this.isArray(value))
            return TYPE.ARRAY;
        if(this.isFunction(value))
            return TYPE.FUNCTION;
        if(this.isObject(value))
            return TYPE.OBJECT;
        if(this.isBoolean(value))
            return TYPE.BOOLEAN;
        if(this.isNull(value))
            return TYPE.NULL;
        if(this.isUndefined(value))
            return TYPE.UNDEFINED;
    },
    isEmpty(value: any){
        switch(this.getType(value)){
            case TYPE.STRING: {
                return !!value.trim().length
            }
            case TYPE.OBJECT: {
                return JSON.stringify(value) === '{}'
            }
            case TYPE.ARRAY: {
                return !!value.length
            }
            default:{
                if(this.isNull(value) || this.isUndefined(value))
                    return true;
            }
        }

        return false
    }
};

export default TypeHelper