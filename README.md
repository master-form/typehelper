# TypeHelper

Simple helper for check js type

## Install

```code
npm i @zidadindimon/js-typehelper
```
## Usage

check type example:

```javascript
import TypeHelper from "@zidadindimon/js-typehelper"
...
let data = [];
TypeHelper.isArray(data) // return true
TypeHelper.isObject(data) //return false
```

get type example:

```javascript
import TypeHelper, {TYPE} from "@zidadindimon/js-typehelper"
TypeHelper.getType(data) //return  TYPE.ARRAY
```

check data is empty:

```javascript
import TypeHelper from "@zidadindimon/js-typehelper"
TypeHelper.isEmpty({}) //return true
TypeHelper.isEmpty([]) //return true
```
